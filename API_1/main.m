//
//  main.m
//  API_1
//
//  Created by Koberskiy on 5/1/16.
//  Copyright © 2016 Koberskiy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
