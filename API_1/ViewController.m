//
//  ViewController.m
//  API_1
//
//  Created by Koberskiy on 5/1/16.
//  Copyright © 2016 Koberskiy. All rights reserved.
//

#import "ViewController.h"
#import "SeverMeneger.h"

@interface ViewController ()

@property (strong, nonatomic) NSMutableArray *friendsArray;

@end

@implementation ViewController

static NSInteger friendsInRequest = 5;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.friendsArray = [NSMutableArray array];
    
    [self getFriendsFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - API
- (void) getFriendsFromServer {
    [[SeverMeneger sharedMeneger]
            getFriendsWithOffset:[self.friendsArray count]
                           count:friendsInRequest
                        onSucces:^(NSArray *friends) {
                            [self.friendsArray addObjectsFromArray:friends];
                            
                            NSMutableArray *newPath = [NSMutableArray array];
                            for (int i = (int)[self.friendsArray count] - (int) [friends count]; i < [self.friendsArray count]; i++) {
                                [newPath addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                            }
                            [self.tableView beginUpdates];
                            [self.tableView insertRowsAtIndexPaths:newPath withRowAnimation:UITableViewRowAnimationTop];
                            [self.tableView endUpdates];
                        }
                         onFaile:^(NSError *error, NSInteger statusCode) {
                            NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode	);
                         }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.friendsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:identifier];
    }
    return cell;
}

@end
