//
//  SeverMeneger.m
//  API_1
//
//  Created by Koberskiy on 5/1/16.
//  Copyright © 2016 Koberskiy. All rights reserved.
//

#import "SeverMeneger.h"
#import "AFNetworking.h"

@interface SeverMeneger()

@property (strong, nonatomic) AFHTTPSessionManager *requestOperationManager;

@end

@implementation SeverMeneger

+ (SeverMeneger*)sharedMeneger{
    static SeverMeneger* meneger = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once (&onceToken, ^{
        meneger = [[SeverMeneger alloc]init];
    });
    return meneger;
}

- (id)init {
    self = [super init];
    if(self){
        NSURL *url = [NSURL URLWithString:@"https://api.vk.com/method/"];
        self.requestOperationManager = [[AFHTTPSessionManager alloc]initWithBaseURL:url];
    }
    return self;
}

- (void) getFriendsWithOffset: (NSInteger) offset
                        count:(NSInteger) count
                     onSucces:(void(^)(NSArray* friends))succes
                      onFaile:(void(^)(NSError* error, NSInteger statusCode))failure{
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:<#(nonnull id), ...#>, nil];
    
    [self.requestOperationManager GET:@"friends.get"
                           parameters:nil
                             progress:nil
                              success:^(NSURLSessionTask *task, id responseObject) {
                                  NSLog(@"JSON: %@", responseObject);
                              } failure:^(NSURLSessionTask *operation, NSError *error) {
                                  NSLog(@"Error: %@", error);
                              }];
    
    //@"https://api.vk.com/method/friends.get";
    
    /*
     friends.get
     
     user_id
     
     order
     list_id
     count
     offset
     name_case nom
     photo_medium
     */
}


@end
