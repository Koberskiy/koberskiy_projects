//
//  SeverMeneger.h
//  API_1
//
//  Created by Koberskiy on 5/1/16.
//  Copyright © 2016 Koberskiy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SeverMeneger : NSObject

+ (SeverMeneger*)sharedMeneger;

- (void) getFriendsWithOffset: (NSInteger) offset
                        count:(NSInteger) count
                     onSucces:(void(^)(NSArray* friends))succes
                      onFaile:(void(^)(NSError* error, NSInteger statusCode))failure;

@end
